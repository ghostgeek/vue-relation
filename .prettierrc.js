module.exports = {
    printWidth: 200, // 每行代码长度（默认80）
    tabWidth: 4, // 每个tab相当于多少个空格（默认2）
    singleQuote: true, // 使用单引号（默认false）
    semi: true, // 声明结尾使用分号(默认true)
    trailingComma: 'none' // 多行使用拖尾逗号（默认none）
};
