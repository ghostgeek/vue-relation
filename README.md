 **注：推荐使用官方版本 https://github.com/seeksdream/relation-graph，此版本仅供学习。
** 
# sf-relation

## 使用方法

1. 引入 sf-relation

    `yarn add sf-relation`

2. 项目中引入

    `import 'sf-relation/sf-relation.min.css';`

    `import SeeksRelationGraph from 'sf-relation';`

3. vue3 样例代码

```
<template>
    <div>
        <div style="height:calc(100vh - 50px);">
            <RelationGraph ref="seeksRelationGraph" :options="graphOptions" :on-node-click="onNodeClick" :on-line-click="onLineClick" />
        </div>
    </div>
</template>

<script>
import 'sf-relation/sf-relation.min.css';
import SeeksRelationGraph from 'sf-relation';
export default defineComponent({
    name: 'App',
    components: { SeeksRelationGraph },
    setup() {
        const seeksRelationGraph = ref(null);
        let graphOptions = reactive({
            backgrounImage: 'http://ai-mark.cn/images/ai-mark-desc.png',
            backgrounImageNoRepeat: true,
            layouts: [
                {
                    label: '中心',
                    layoutName: 'tree',
                    layoutClassName: 'seeks-layout-center',
                    defaultJunctionPoint: 'border',
                    defaultNodeShape: 0,
                    defaultLineShape: 1,
                    from: 'left',
                    max_per_width: '300',
                    min_per_height: '40'
                }
            ],
            defaultLineMarker: {
                markerWidth: 12,
                markerHeight: 12,
                refX: 6,
                refY: 6,
                data: 'M2,2 L10,6 L2,10 L6,6 L2,2'
            },
            defaultNodeShape: 1,
            defaultNodeWidth: '100',
            defaultLineShape: 2,
            defaultJunctionPoint: 'lr',
            defaultNodeBorderWidth: 0,
            defaultLineColor: 'rgba(0, 186, 189, 1)',
            defaultNodeColor: 'rgba(0, 206, 209, 1)'
        });

        onMounted(() => {
            showSeeksGraph();
        });

        function showSeeksGraph() {
            var __graph_json_data = {
                rootId: 'a',
                nodes: [
                    { id: 'a', text: 'a' },
                    { id: 'b', text: 'b' },
                    { id: 'b1', text: 'b1' },
                    { id: 'b1-1', text: 'b1-1' },
                    { id: 'b1-2', text: 'b1-2' },
                    { id: 'b1-3', text: 'b1-3' },
                    { id: 'b1-4', text: 'b1-4' },
                    { id: 'b1-5', text: 'b1-5' },
                    { id: 'b1-6', text: 'b1-6' },
                    { id: 'b2', text: 'b2' },
                    { id: 'b2-1', text: 'b2-1' },
                    { id: 'b2-2', text: 'b2-2' },
                    { id: 'b2-3', text: 'b2-3' },
                    { id: 'b2-4', text: 'b2-4' },
                    { id: 'b3', text: 'b3' },
                    { id: 'b3-1', text: 'b3-1' },
                    { id: 'b3-2', text: 'b3-2' },
                    { id: 'b3-3', text: 'b3-3' },
                    { id: 'b3-4', text: 'b3-4' },
                    { id: 'b3-5', text: 'b3-5' },
                    { id: 'b3-6', text: 'b3-6' },
                    { id: 'b3-7', text: 'b3-7' },
                    { id: 'b4', text: 'b4' },
                    { id: 'b4-1', text: 'b4-1' },
                    { id: 'b4-2', text: 'b4-2' },
                    { id: 'b4-3', text: 'b4-3' },
                    { id: 'b4-4', text: 'b4-4' },
                    { id: 'b4-5', text: 'b4-5' },
                    { id: 'b4-6', text: 'b4-6' },
                    { id: 'b4-7', text: 'b4-7' },
                    { id: 'b4-8', text: 'b4-8' },
                    { id: 'b4-9', text: 'b4-9' },
                    { id: 'b5', text: 'b5' },
                    { id: 'b5-1', text: 'b5-1' },
                    { id: 'b5-2', text: 'b5-2' },
                    { id: 'b5-3', text: 'b5-3' },
                    { id: 'b5-4', text: 'b5-4' },
                    { id: 'b6', text: 'b6' },
                    { id: 'b6-1', text: 'b6-1' },
                    { id: 'b6-2', text: 'b6-2' },
                    { id: 'b6-3', text: 'b6-3' },
                    { id: 'b6-4', text: 'b6-4' },
                    { id: 'b6-5', text: 'b6-5' },
                    { id: 'c', text: 'c' },
                    { id: 'c1', text: 'c1' },
                    { id: 'c1-1', text: 'c1-1' },
                    { id: 'c1-2', text: 'c1-2' },
                    { id: 'c1-3', text: 'c1-3' },
                    { id: 'c1-4', text: 'c1-4' },
                    { id: 'c1-5', text: 'c1-5' },
                    { id: 'c1-6', text: 'c1-6' },
                    { id: 'c1-7', text: 'c1-7' },
                    { id: 'c2', text: 'c2' },
                    { id: 'c2-1', text: 'c2-1' },
                    { id: 'c2-2', text: 'c2-2' },
                    { id: 'c3', text: 'c3' },
                    { id: 'c3-1', text: 'c3-1' },
                    { id: 'c3-2', text: 'c3-2' },
                    { id: 'c3-3', text: 'c3-3' },
                    { id: 'd', text: 'd' },
                    { id: 'd1', text: 'd1' },
                    { id: 'd1-1', text: 'd1-1' },
                    { id: 'd1-2', text: 'd1-2' },
                    { id: 'd1-3', text: 'd1-3' },
                    { id: 'd1-4', text: 'd1-4' },
                    { id: 'd1-5', text: 'd1-5' },
                    { id: 'd1-6', text: 'd1-6' },
                    { id: 'd1-7', text: 'd1-7' },
                    { id: 'd1-8', text: 'd1-8' },
                    { id: 'd2', text: 'd2' },
                    { id: 'd2-1', text: 'd2-1' },
                    { id: 'd2-2', text: 'd2-2' },
                    { id: 'd3', text: 'd3' },
                    { id: 'd3-1', text: 'd3-1' },
                    { id: 'd3-2', text: 'd3-2' },
                    { id: 'd3-3', text: 'd3-3' },
                    { id: 'd3-4', text: 'd3-4' },
                    { id: 'd3-5', text: 'd3-5' },
                    { id: 'd4', text: 'd4' },
                    { id: 'd4-1', text: 'd4-1' },
                    { id: 'd4-2', text: 'd4-2' },
                    { id: 'd4-3', text: 'd4-3' },
                    { id: 'd4-4', text: 'd4-4' },
                    { id: 'd4-5', text: 'd4-5' },
                    { id: 'd4-6', text: 'd4-6' },
                    { id: 'e', text: 'e' },
                    { id: 'e1', text: 'e1' },
                    { id: 'e1-1', text: 'e1-1' },
                    { id: 'e1-2', text: 'e1-2' },
                    { id: 'e1-3', text: 'e1-3' },
                    { id: 'e1-4', text: 'e1-4' },
                    { id: 'e1-5', text: 'e1-5' },
                    { id: 'e1-6', text: 'e1-6' },
                    { id: 'e2', text: 'e2' },
                    { id: 'e2-1', text: 'e2-1' },
                    { id: 'e2-2', text: 'e2-2' },
                    { id: 'e2-3', text: 'e2-3' },
                    { id: 'e2-4', text: 'e2-4' },
                    { id: 'e2-5', text: 'e2-5' },
                    { id: 'e2-6', text: 'e2-6' },
                    { id: 'e2-7', text: 'e2-7' },
                    { id: 'e2-8', text: 'e2-8' },
                    { id: 'e2-9', text: 'e2-9' }
                ],
                links: [
                    { from: 'a', to: 'b' },
                    { from: 'b', to: 'b1' },
                    { from: 'b1', to: 'b1-1' },
                    { from: 'b1', to: 'b1-2' },
                    { from: 'b1', to: 'b1-3' },
                    { from: 'b1', to: 'b1-4' },
                    { from: 'b1', to: 'b1-5' },
                    { from: 'b1', to: 'b1-6' },
                    { from: 'b', to: 'b2' },
                    { from: 'b2', to: 'b2-1' },
                    { from: 'b2', to: 'b2-2' },
                    { from: 'b2', to: 'b2-3' },
                    { from: 'b2', to: 'b2-4' },
                    { from: 'b', to: 'b3' },
                    { from: 'b3', to: 'b3-1' },
                    { from: 'b3', to: 'b3-2' },
                    { from: 'b3', to: 'b3-3' },
                    { from: 'b3', to: 'b3-4' },
                    { from: 'b3', to: 'b3-5' },
                    { from: 'b3', to: 'b3-6' },
                    { from: 'b3', to: 'b3-7' },
                    { from: 'b', to: 'b4' },
                    { from: 'b4', to: 'b4-1' },
                    { from: 'b4', to: 'b4-2' },
                    { from: 'b4', to: 'b4-3' },
                    { from: 'b4', to: 'b4-4' },
                    { from: 'b4', to: 'b4-5' },
                    { from: 'b4', to: 'b4-6' },
                    { from: 'b4', to: 'b4-7' },
                    { from: 'b4', to: 'b4-8' },
                    { from: 'b4', to: 'b4-9' },
                    { from: 'b', to: 'b5' },
                    { from: 'b5', to: 'b5-1' },
                    { from: 'b5', to: 'b5-2' },
                    { from: 'b5', to: 'b5-3' },
                    { from: 'b5', to: 'b5-4' },
                    { from: 'b', to: 'b6' },
                    { from: 'b6', to: 'b6-1' },
                    { from: 'b6', to: 'b6-2' },
                    { from: 'b6', to: 'b6-3' },
                    { from: 'b6', to: 'b6-4' },
                    { from: 'b6', to: 'b6-5' },
                    { from: 'a', to: 'c' },
                    { from: 'c', to: 'c1' },
                    { from: 'c1', to: 'c1-1' },
                    { from: 'c1', to: 'c1-2' },
                    { from: 'c1', to: 'c1-3' },
                    { from: 'c1', to: 'c1-4' },
                    { from: 'c1', to: 'c1-5' },
                    { from: 'c1', to: 'c1-6' },
                    { from: 'c1', to: 'c1-7' },
                    { from: 'c', to: 'c2' },
                    { from: 'c2', to: 'c2-1' },
                    { from: 'c2', to: 'c2-2' },
                    { from: 'c', to: 'c3' },
                    { from: 'c3', to: 'c3-1' },
                    { from: 'c3', to: 'c3-2' },
                    { from: 'c3', to: 'c3-3' },
                    { from: 'a', to: 'd' },
                    { from: 'd', to: 'd1' },
                    { from: 'd1', to: 'd1-1' },
                    { from: 'd1', to: 'd1-2' },
                    { from: 'd1', to: 'd1-3' },
                    { from: 'd1', to: 'd1-4' },
                    { from: 'd1', to: 'd1-5' },
                    { from: 'd1', to: 'd1-6' },
                    { from: 'd1', to: 'd1-7' },
                    { from: 'd1', to: 'd1-8' },
                    { from: 'd', to: 'd2' },
                    { from: 'd2', to: 'd2-1' },
                    { from: 'd2', to: 'd2-2' },
                    { from: 'd', to: 'd3' },
                    { from: 'd3', to: 'd3-1' },
                    { from: 'd3', to: 'd3-2' },
                    { from: 'd3', to: 'd3-3' },
                    { from: 'd3', to: 'd3-4' },
                    { from: 'd3', to: 'd3-5' },
                    { from: 'd', to: 'd4' },
                    { from: 'd4', to: 'd4-1' },
                    { from: 'd4', to: 'd4-2' },
                    { from: 'd4', to: 'd4-3' },
                    { from: 'd4', to: 'd4-4' },
                    { from: 'd4', to: 'd4-5' },
                    { from: 'd4', to: 'd4-6' },
                    { from: 'a', to: 'e' },
                    { from: 'e', to: 'e1' },
                    { from: 'e1', to: 'e1-1' },
                    { from: 'e1', to: 'e1-2' },
                    { from: 'e1', to: 'e1-3' },
                    { from: 'e1', to: 'e1-4' },
                    { from: 'e1', to: 'e1-5' },
                    { from: 'e1', to: 'e1-6' },
                    { from: 'e', to: 'e2' },
                    { from: 'e2', to: 'e2-1' },
                    { from: 'e2', to: 'e2-2' },
                    { from: 'e2', to: 'e2-3' },
                    { from: 'e2', to: 'e2-4' },
                    { from: 'e2', to: 'e2-5' },
                    { from: 'e2', to: 'e2-6' },
                    { from: 'e2', to: 'e2-7' },
                    { from: 'e2', to: 'e2-8' },
                    { from: 'e2', to: 'e2-9' }
                ]
            };

            seeksRelationGraph.value.setJsonData(__graph_json_data, (seeksRGGraph) => {
                // Called when the relation-graph is completed
                console.log('[sf-relation]完成', seeksRGGraph);
            });
        }

        function onNodeClick(nodeObject, $event) {
            console.log('onNodeClick:', nodeObject);
            console.log('onNodeClick:', $event);
        }
        function onLineClick(lineObject, $event) {
            console.log('onLineClick:', lineObject);
            console.log('onNodeClick:', $event);
        }

        return {
            graphOptions,
            onNodeClick,
            onLineClick,
            seeksRelationGraph
        };
    }
});
</script>
```

4. 图片

    ![avatar](../public/demo.png)

## 接口

### 1. graph 参数

<div data-v-62740fd4="" data-v-92459f82="">
  <div data-v-62740fd4="">
    <div data-v-62740fd4="" class="c-doc-title">Arributes / 选项:</div>
    <div
      data-v-62740fd4=""
      class="el-table el-table--fit el-table--enable-row-hover el-table--enable-row-transition el-table--medium"
      style="width: 100%"
    >
      <div class="el-table__body-wrapper is-scrolling-none">
        <table
          cellspacing="0"
          cellpadding="0"
          border="0"
          class="el-table__body"
          style="width: 1137px"
        >
        <thead class="has-gutter">
            <tr class="">
              <th
                colspan="1"
                rowspan="1"
                class="el-table_29_column_111 is-leaf"
              >
                <div class="cell">参数</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_29_column_112 is-leaf"
              >
                <div class="cell">说明</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_29_column_113 is-leaf"
              >
                <div class="cell">类型</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_29_column_114 is-leaf"
              >
                <div class="cell">必选/非必选</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_29_column_115 is-leaf"
              >
                <div class="cell">默认值</div>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell"><div data-v-62740fd4="">options</div></div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">
                    图谱配置，一个对象，里面包含了具体的选项：
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">Object</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">是</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span>backgrounImage
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">
                    图谱水印url,如：http://ai-mark.cn/images/ai-mark-desc.png
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span
                    >backgrounImageNoRepeat
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">
                    只在右下角显示水印，不重复显示水印
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">boolean</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell">true</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span>allowShowMiniToolBar
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">是否显示工具栏</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">boolean</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell">true</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span>allowShowMiniView
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">是否显示缩略图</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">boolean</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell">false</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span
                    >allowShowMiniNameFilter
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">是否显示搜索框</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">boolean</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell">true</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span>allowSwitchLineShape
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">
                    是否在工具栏中显示切换线条形状的按钮
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">boolean</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell">true</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span
                    >allowSwitchJunctionPoint
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">
                    是否在工具栏中显示切换连接点位置的按钮
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">boolean</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell">false</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span>disableZoom
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">
                    是否禁用图谱的缩放功能，这里特指通过鼠标滚轮进行缩放的功能，禁用后你依然可以通过图谱工具栏按钮进行缩放
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">boolean</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell">false</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span>disableDragNode
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">是否禁用图谱中节点的拖动</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">boolean</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell">false</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span
                    >moveToCenterWhenResize
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">
                    当图谱的大小发生变化时，是否重新让图谱的内容看起来居中
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">boolean</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell">true</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span>defaultFocusRootNode
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">默认为根节点添加一个被选中的样式</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">boolean</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell">true</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span>allowShowZoomMenu
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">
                    是否在右侧菜单栏显示放大缩小的按钮，此设置和disableZoom不冲突
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">boolean</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell">true</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span>isMoveByParentNode
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">是否在拖动节点后让子节点跟随</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">boolean</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell">false</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span>hideNodeContentByZoom
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">是否根据缩放比例隐藏节点内容</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">boolean</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell">false</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span>defaultNodeShape
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">默认的节点形状，0:圆形；1:矩形</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell">1</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span>defaultNodeColor
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">默认的节点背景颜色</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span>defaultNodeFontColor
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">默认的节点文字颜色</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span
                    >defaultNodeBorderColor
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">默认的节点边框颜色</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span
                    >defaultNodeBorderWidth
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">默认的节点边框粗细（像素）</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell">1</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span>defaultNodeWidth
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">默认的节点宽度</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell">自动</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span>defaultNodeHeight
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">默认的节点高度</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell">自动</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span>defaultJunctionPoint
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">
                    默认的连线与节点接触的方式（border:边缘/ltrb:上下左右/tb:上下/lr:左右）<span
                      style="color: red"
                      >当布局为树状布局时应使用tb或者lr，这样才会好看</span
                    >
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell">自动</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span
                    >defaultExpandHolderPosition
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">
                    默认的节点展开/关闭按钮位置（left/top/right/bottom）
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell">hide</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span>defaultLineColor
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">默认的线条颜色</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span>defaultLineWidth
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">默认的线条粗细（像素）</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell">1</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span>defaultLineShape
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">
                    默认的线条样式（1:直线/2:样式2/3:样式3/4:折线/5:样式5/6:样式6）<a
                      href="#/demo/line"
                      >使用示例</a
                    >
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span>defaultLineMarker
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">
                    默认的线条箭头样式，示例参考：<a href="#/options-tools"
                      >配置工具</a
                    >中的选项：连线箭头样式
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">Object</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span>defaultShowLineLabel
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">默认是否显示连线文字</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">boolean</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell">true</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">
                    <span class="c-ops-tag">options.</span>layouts
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">
                    布局方式，可以设置多个布局，用于切换<a
                      href="#/demo/adv-multi-layout"
                      >查看布局参数配置说明</a
                    >
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">Object/Array</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell">中心布局</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">on-node-click</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">
                    标签参数：点击节点时触发的方法，参数：(nodeObject, $event)
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">function</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_29_column_111">
                <div class="cell">
                  <div data-v-62740fd4="">on-line-click</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_112">
                <div class="cell">
                  <div data-v-62740fd4="">
                    标签参数：点击连线时触发的方法，参数：(lineObject, $event)
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_113">
                <div class="cell">function</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_114">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_29_column_115">
                <div class="cell"></div>
              </td>
            </tr>
            <!---->
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div data-v-62740fd4="">
    <div data-v-62740fd4="" class="c-doc-title">
      Runtime-properties / 运行时属性:
    </div>
    <div
      data-v-62740fd4=""
      class="el-table el-table--fit el-table--enable-row-hover el-table--enable-row-transition el-table--medium"
      style="width: 100%"
    >
      <div class="el-table__body-wrapper is-scrolling-none">
        <table
          cellspacing="0"
          cellpadding="0"
          border="0"
          class="el-table__body"
          style="width: 1137px"
        >
          <thead class="has-gutter">
            <tr class="">
              <th
                colspan="1"
                rowspan="1"
                class="el-table_30_column_116 is-leaf"
              >
                <div class="cell">属性名</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_30_column_117 is-leaf"
              >
                <div class="cell">说明</div>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_30_column_116">
                <div class="cell">
                  <div data-v-62740fd4="">graphSetting</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_30_column_117">
                <div class="cell">
                  <div data-v-62740fd4="">
                    图片会根据options生成一个包含默认值的完整配置对象，它就是graphSetting，可以通过this.$refs.seeksRelationGraph.graphSetting来获取；<span
                      style="color: red"
                      >你还可以从这个对象中获取当前图谱的可见区域大小、画布大小、画布偏移量、当前布局器等运行时对象。</span
                    >
                  </div>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div data-v-62740fd4="">
    <div data-v-62740fd4="" class="c-doc-title">Methods / 方法:</div>
    <div
      data-v-62740fd4=""
      class="el-table el-table--fit el-table--enable-row-hover el-table--enable-row-transition el-table--medium"
      style="width: 100%"
    >
      <div class="el-table__body-wrapper is-scrolling-none">
        <table
          cellspacing="0"
          cellpadding="0"
          border="0"
          class="el-table__body"
          style="width: 1137px"
        >
          <thead class="has-gutter">
            <tr class="">
              <th
                colspan="1"
                rowspan="1"
                class="el-table_31_column_118 is-leaf"
              >
                <div class="cell">事件名</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_31_column_119 is-leaf"
              >
                <div class="cell">说明</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_31_column_120 is-leaf"
              >
                <div class="cell">参数</div>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_31_column_118">
                <div class="cell">
                  <div data-v-62740fd4="">setOptions(options, callback)</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_119">
                <div class="cell">
                  <div data-v-62740fd4="">
                    设置/重新设置图谱的选项,options：图谱选项,不能为空;callback:当设置完成后的回调函数,可以为空;options选项设置方法示例：&lt;SeeksRelationGraph
                    ref="seeksRelationGraph" :options="userGraphSetting" /&gt;
                    更改设置：seeksRelationGraph.setOptions(newOptions,
                    callback);
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_120">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_31_column_118">
                <div class="cell">
                  <div data-v-62740fd4="">setJsonData(jsonData, callback)</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_119">
                <div class="cell">
                  <div data-v-62740fd4="">
                    设置/重新设置图谱中的数据,jsonData：图谱数据,不能为空，jsonData中需要包含三要素（rootId、nodes、links）<a
                      href="#/demo/adv-expand"
                      >数据格式示例</a
                    >;callback:当设置完成后的回调函数,可以为空;
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_120">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_31_column_118">
                <div class="cell">
                  <div data-v-62740fd4="">
                    appendJsonData(jsonData, callback)
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_119">
                <div class="cell">
                  <div data-v-62740fd4="">
                    向图谱中追加数据,jsonData：图谱数据,不能为空，<a
                      href="#/demo/adv-dynamic-data"
                      >数据格式示例</a
                    >;callback:当设置完成后的回调函数,可以为空;
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_120">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_31_column_118">
                <div class="cell"><div data-v-62740fd4="">refresh()</div></div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_119">
                <div class="cell">
                  <div data-v-62740fd4="">
                    刷新布局，你可以通过getNodes()获取当前图谱中的节点，并通过节点的isHide属性隐藏一些节点，再调用refresh()方法可以根据依然显示的节点重新布局图谱;或者在动态向图谱中添加数据候刷新布局;当你的图片默认默认状态是不可见的时，在切换到可见状态下后可能会显示不正常，这时你调用一下refresh()方法可以让图片正确显示。<span
                      style="color: red"
                      >总之一句话：当图谱中的节点看起来不正常时，你都可以调用refresh方法来让布局器重新为节点分配位置。</span
                    >
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_120">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_31_column_118">
                <div class="cell">
                  <div data-v-62740fd4="">focusRootNode()</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_119">
                <div class="cell">
                  <div data-v-62740fd4="">选中图谱的根节点居中并选中;</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_120">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_31_column_118">
                <div class="cell">
                  <div data-v-62740fd4="">focusNodeById(nodeId)</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_119">
                <div class="cell">
                  <div data-v-62740fd4="">
                    根据节点id在图谱中选中该节点并居中;
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_120">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_31_column_118">
                <div class="cell">
                  <div data-v-62740fd4="">getNodeById(nodeId)</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_119">
                <div class="cell">
                  <div data-v-62740fd4="">根据节点id获取节点对象;</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_120">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_31_column_118">
                <div class="cell">
                  <div data-v-62740fd4="">removeNodeById(nodeId)</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_119">
                <div class="cell">
                  <div data-v-62740fd4="">
                    移除指定id对应的节点，彻底移除，移除element和数据对象;
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_120">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_31_column_118">
                <div class="cell">
                  <div data-v-62740fd4="">downloadAsImage(format)</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_119">
                <div class="cell">
                  <div data-v-62740fd4="">
                    下载图片，将当前图谱导出为图片，format可以为：png/jpg，默认为png;
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_120">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_31_column_118">
                <div class="cell"><div data-v-62740fd4="">getNodes()</div></div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_119">
                <div class="cell">
                  <div data-v-62740fd4="">
                    获取图谱中所有的节点对象，可以直接修改该对象的属性，这些对象不能用于JSON序列化;
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_120">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_31_column_118">
                <div class="cell"><div data-v-62740fd4="">getLines()</div></div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_119">
                <div class="cell">
                  <div data-v-62740fd4="">
                    获取图谱中所有的连线对象，可以直接修改该对象的属性，这些对象不能用于JSON序列化;
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_120">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_31_column_118">
                <div class="cell">
                  <div data-v-62740fd4="">getGraphJsonData()</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_119">
                <div class="cell">
                  <div data-v-62740fd4="">
                    获取当前图谱的节点、关系数据的json数据;
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_120">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_31_column_118">
                <div class="cell">
                  <div data-v-62740fd4="">getGraphJsonOptions()</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_119">
                <div class="cell">
                  <div data-v-62740fd4="">获取当前图谱的完整的配置信息;</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_31_column_120">
                <div class="cell"></div>
              </td>
            </tr>
            <!---->
          </tbody>
        </table>
        <!----><!---->
      </div>
      <!----><!----><!----><!---->
      <div class="el-table__column-resize-proxy" style="display: none"></div>
    </div>
  </div>
  <div data-v-62740fd4="">
    <div data-v-62740fd4="" class="c-doc-title">Events / 事件:</div>
    <div
      data-v-62740fd4=""
      class="el-table el-table--fit el-table--enable-row-hover el-table--enable-row-transition el-table--medium"
      style="width: 100%"
    >
      <div class="el-table__body-wrapper is-scrolling-none">
        <table
          cellspacing="0"
          cellpadding="0"
          border="0"
          class="el-table__body"
          style="width: 1137px"
        >
          <thead class="has-gutter">
            <tr class="">
              <th
                colspan="1"
                rowspan="1"
                class="el-table_32_column_121 is-leaf"
              >
                <div class="cell">事件名</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_32_column_122 is-leaf"
              >
                <div class="cell">说明</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_32_column_123 is-leaf"
              >
                <div class="cell">参数</div>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_32_column_121">
                <div class="cell">on-node-click</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_32_column_122">
                <div class="cell">
                  <div data-v-62740fd4="">
                    点击节点事件，注意：请使用&lt;graph
                    :on-node-click=functionName /&gt;的方式来绑定节点事件,<a
                      href="#/demo/adv-effect"
                      >使用示例</a
                    >
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_32_column_123">
                <div class="cell">(nodeObject, $event)</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_32_column_121">
                <div class="cell">on-node-expand</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_32_column_122">
                <div class="cell">
                  <div data-v-62740fd4="">
                    展开节点事件，仅当节点的【展开/收缩】按钮可用时有效，注意：请使用&lt;graph
                    :on-node-expand=functionName /&gt;的方式来绑定事件,<a
                      href="/#/demo/scene-org"
                      >使用示例</a
                    >
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_32_column_123">
                <div class="cell">(nodeObject, $event)</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_32_column_121">
                <div class="cell">on-node-collapse</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_32_column_122">
                <div class="cell">
                  <div data-v-62740fd4="">
                    收缩节点事件，仅当节点的【展开/收缩】按钮可用时有效，注意：请使用&lt;graph
                    :on-node-collapse=functionName /&gt;的方式来绑定事件,<a
                      href="/#/demo/scene-org"
                      >使用示例</a
                    >
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_32_column_123">
                <div class="cell">(nodeObject, $event)</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_32_column_121">
                <div class="cell">on-line-click</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_32_column_122">
                <div class="cell">
                  <div data-v-62740fd4="">
                    点击线条事件，注意：请使用&lt;graph
                    :on-line-click=functionName /&gt;的方式来绑定关系线事件,<a
                      href="#/demo/adv-effect"
                      >使用示例</a
                    >
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_32_column_123">
                <div class="cell">(lineObject, $event)</div>
              </td>
            </tr>
            <!---->
          </tbody>
        </table>
        <!----><!---->
      </div>
      <!----><!----><!----><!---->
      <div class="el-table__column-resize-proxy" style="display: none"></div>
    </div>
  </div>
  <div data-v-62740fd4="">
    <div data-v-62740fd4="" class="c-doc-title">Slot / 插槽:</div>
    <div
      data-v-62740fd4=""
      class="el-table el-table--fit el-table--enable-row-hover el-table--enable-row-transition el-table--medium"
      style="width: 100%"
    >
      <div class="el-table__body-wrapper is-scrolling-none">
        <table
          cellspacing="0"
          cellpadding="0"
          border="0"
          class="el-table__body"
          style="width: 1137px"
        >
          <thead class="has-gutter">
            <tr class="">
              <th
                colspan="1"
                rowspan="1"
                class="el-table_33_column_124 is-leaf"
              >
                <div class="cell">事件名</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_33_column_125 is-leaf"
              >
                <div class="cell">说明</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_33_column_126 is-leaf"
              >
                <div class="cell">参数</div>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_33_column_124">
                <div class="cell">node</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_33_column_125">
                <div class="cell">
                  <div data-v-62740fd4="">
                    自定义节点内容的插槽，通过此功能可以完全自定义节点的样式，实现图片节点、闪烁节点等等你想要的效果。<a
                      href="#/demo/adv-slot"
                      >使用示例</a
                    >
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_33_column_126">
                <div class="cell">{ node }</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_33_column_124">
                <div class="cell">bottomPanel</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_33_column_125">
                <div class="cell">
                  <div data-v-62740fd4="">
                    自定义图谱底部区域内容的插槽，通过此功能可以在图谱的底部显示一些内容，比如筛选区域，图例说明等。
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_33_column_126">
                <div class="cell">{ graph }</div>
              </td>
            </tr>
            <!---->
          </tbody>
        </table>
        <!----><!---->
      </div>
      <!----><!----><!----><!---->
    </div>
  </div>
</div>

### 2. node 参数

<div data-v-1994be18="" data-v-92459f82="">
  <div data-v-1994be18="">
    <div data-v-1994be18="" class="c-doc-title">Arributes / 选项:</div>
    <div
      data-v-1994be18=""
      class="el-table el-table--fit el-table--enable-row-hover el-table--enable-row-transition el-table--medium"
      style="width: 100%"
    >
      <div class="el-table__body-wrapper is-scrolling-none">
        <table
          cellspacing="0"
          cellpadding="0"
          border="0"
          class="el-table__body"
          style="width: 1137px"
        >
          <thead class="has-gutter">
            <tr class="">
              <th
                colspan="1"
                rowspan="1"
                class="el-table_34_column_127 is-leaf"
              >
                <div class="cell">参数</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_34_column_128 is-leaf"
              >
                <div class="cell">说明</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_34_column_129 is-leaf"
              >
                <div class="cell">类型</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_34_column_130 is-leaf"
              >
                <div class="cell">必选/非必选</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_34_column_131 is-leaf"
              >
                <div class="cell">默认值</div>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_34_column_127">
                <div class="cell">id</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_128">
                <div class="cell">
                  <div data-v-1994be18="">节点id，不能重复，重复会被忽略</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_129">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_130">
                <div class="cell">是</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_131">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_34_column_127">
                <div class="cell">text</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_128">
                <div class="cell"><div data-v-1994be18="">节点名称</div></div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_129">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_130">
                <div class="cell">是</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_131">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_34_column_127">
                <div class="cell">styleClass</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_128">
                <div class="cell">
                  <div data-v-1994be18="">节点样式class</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_129">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_130">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_131">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_34_column_127">
                <div class="cell">color</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_128">
                <div class="cell">
                  <div data-v-1994be18="">节点背景颜色</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_129">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_130">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_131">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_34_column_127">
                <div class="cell">fontColor</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_128">
                <div class="cell">
                  <div data-v-1994be18="">节点文字颜色</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_129">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_130">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_131">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_34_column_127">
                <div class="cell">borderWidth</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_128">
                <div class="cell">
                  <div data-v-1994be18="">节点边框粗细（像素）</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_129">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_130">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_131">
                <div class="cell">1</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_34_column_127">
                <div class="cell">borderColor</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_128">
                <div class="cell">
                  <div data-v-1994be18="">节点边框颜色</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_129">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_130">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_131">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_34_column_127">
                <div class="cell">nodeShape</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_128">
                <div class="cell">
                  <div data-v-1994be18="">
                    节点形状，0:圆形；1:矩形;<a href="#/demo/node">使用示例</a>
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_129">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_130">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_131">
                <div class="cell">1</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_34_column_127">
                <div class="cell">width</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_128">
                <div class="cell"><div data-v-1994be18="">节点宽度</div></div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_129">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_130">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_131">
                <div class="cell">自动</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_34_column_127">
                <div class="cell">height</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_128">
                <div class="cell"><div data-v-1994be18="">节点高度</div></div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_129">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_130">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_131">
                <div class="cell">自动</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_34_column_127">
                <div class="cell">opacity</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_128">
                <div class="cell">
                  <div data-v-1994be18="">透明度(值范围：0到1,或者0到100)</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_129">
                <div class="cell">number</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_130">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_131">
                <div class="cell">1</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_34_column_127">
                <div class="cell">isHide</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_128">
                <div class="cell">
                  <div data-v-1994be18="">是否隐藏这个节点</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_129">
                <div class="cell">boolean</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_130">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_131">
                <div class="cell">false</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_34_column_127">
                <div class="cell">disableDrag</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_128">
                <div class="cell">
                  <div data-v-1994be18="">是否禁用节点的拖动功能</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_129">
                <div class="cell">boolean</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_130">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_131">
                <div class="cell">false</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_34_column_127">
                <div class="cell">disableDefaultClickEffect</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_128">
                <div class="cell">
                  <div data-v-1994be18="">
                    是否禁用默认的点击选中效果（即使禁用，可以出发自定的节点点击事件）
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_129">
                <div class="cell">boolean</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_130">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_131">
                <div class="cell">false</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_34_column_127">
                <div class="cell">expandHolderPosition</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_128">
                <div class="cell">
                  <div data-v-1994be18="">
                    节点展开/关闭按钮位置（left/top/right/bottom）节点的这个设置会覆盖全局的expandHolderPosition设置，即使改节点没有子节点也可以让其显示展开/收缩按钮，可以实现部分节点显示展开/收缩按钮的效果;<a
                      href="#/demo/adv-expand"
                      >使用示例</a
                    >
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_129">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_130">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_131">
                <div class="cell">hide</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_34_column_127">
                <div class="cell">expanded</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_128">
                <div class="cell">
                  <div data-v-1994be18="">
                    手工设置节点的展开收缩状态;<a href="#/demo/adv-expand"
                      >使用示例</a
                    >
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_129">
                <div class="cell">boolean</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_130">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_131">
                <div class="cell">true</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_34_column_127">
                <div class="cell">fixed</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_128">
                <div class="cell">
                  <div data-v-1994be18="">
                    是否使用固定位置（以当前图谱左上角为0,0的坐标系）;<a
                      href="#/demo/layout-diy"
                      >使用示例</a
                    >
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_129">
                <div class="cell">boolean</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_130">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_131">
                <div class="cell">false</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_34_column_127">
                <div class="cell">x</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_128">
                <div class="cell">
                  <div data-v-1994be18="">
                    x坐标（以当前图谱左上角为0,0的坐标系）,fixed=true时有效
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_129">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_130">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_131">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_34_column_127">
                <div class="cell">y</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_128">
                <div class="cell">
                  <div data-v-1994be18="">
                    y坐标（以当前图谱左上角为0,0的坐标系）,fixed=true时有效
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_129">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_130">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_131">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_34_column_127">
                <div class="cell">innerHTML</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_128">
                <div class="cell">
                  <div data-v-1994be18="">
                    用HTML自定义节点内部的内容，当此属性不为空时，text属性将失效;<a
                      href="#/demo/node"
                      >使用示例</a
                    >
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_129">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_130">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_131">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_34_column_127">
                <div class="cell">html</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_128">
                <div class="cell">
                  <div data-v-1994be18="">
                    用HTML自定义节点，当此属性不为空时，节点的所有样式属性将失效<a
                      href="#/demo/node"
                      >使用示例</a
                    >
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_129">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_130">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_131">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_34_column_127">
                <div class="cell">data</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_128">
                <div class="cell">
                  <div data-v-1994be18="">
                    <span style="color: red"
                      >自定义属性需要放在这里，才能在后续使用中从节点获取，如:{myKey1:'value1',myKey2:'value2'}</span
                    >，示例1：<a href="#/demo/adv-slot"
                      >使用自定义属性作为节点名称/节点图标</a
                    >，示例1：<a href="#/demo/adv-data-filter"
                      >使用自定义属性进行筛选</a
                    >
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_129">
                <div class="cell">Object</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_130">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_34_column_131">
                <div class="cell"></div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div data-v-1994be18="">
    <div data-v-1994be18="" class="c-doc-title">
      Runtime-properties / 运行时属性:
    </div>
    <div
      data-v-1994be18=""
      class="el-table el-table--fit el-table--enable-row-hover el-table--enable-row-transition el-table--medium"
      style="width: 100%"
    >
      <div class="el-table__body-wrapper is-scrolling-none">
        <table
          cellspacing="0"
          cellpadding="0"
          border="0"
          class="el-table__body"
          style="width: 1137px"
        >
          <thead class="has-gutter">
            <tr class="">
              <th
                colspan="1"
                rowspan="1"
                class="el-table_35_column_132 is-leaf"
              >
                <div class="cell">属性名</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_35_column_133 is-leaf"
              >
                <div class="cell">说明</div>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_35_column_132">
                <div class="cell">
                  <div data-v-1994be18="">targetNodes</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_35_column_133">
                <div class="cell">
                  <div data-v-1994be18="">
                    获取与当前节点存在关系的其他所有节点，这些节点中包含的当前节点的父节点和子节点，如果只想获取父节点请使用lot.parent,获取子节点请使用lot.childs
                  </div>
                </div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_35_column_132">
                <div class="cell"><div data-v-1994be18="">lot</div></div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_35_column_133">
                <div class="cell">
                  <div data-v-1994be18="">
                    获取与当前节点的布局信息，其中包含了当前布局器为其设置的坐标、层级、权重、上下级节点等信息
                  </div>
                </div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_35_column_132">
                <div class="cell"><div data-v-1994be18="">lot.parent</div></div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_35_column_133">
                <div class="cell">
                  <div data-v-1994be18="">
                    单独说明lot中的这个属性，他可以获取节点的父节点，这个子节点是由布局器分析出来的父节点，在有循环闭合回路的关系网中是不准确的，他会强行从多个中取一个
                  </div>
                </div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_35_column_132">
                <div class="cell"><div data-v-1994be18="">lot.childs</div></div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_35_column_133">
                <div class="cell">
                  <div data-v-1994be18="">
                    单独说明lot中的这个属性：他可以获取节点的子节点，这个子节点是由布局器分析出来的子节点，在有循环闭合回路的关系网中是不完整的，他会强行剔除一些冲突的子节点
                  </div>
                </div>
              </td>
            </tr>
            <!---->
          </tbody>
        </table>
        <!----><!---->
      </div>
      <!----><!----><!----><!---->
      <div class="el-table__column-resize-proxy" style="display: none"></div>
    </div>
  </div>
</div>

### 3. link参数

<div data-v-99ca356a="" data-v-92459f82="">
  <div
    data-v-99ca356a=""
    style="font-size: 16px; color: rgb(51, 51, 51); line-height: 25px"
  >
    在构建图谱数据时需要两个要素nodes和links，其中的links是指节点之间的关系（link），在图谱内部中将其称之为relation，图谱会根据这些关系(relation)来生成线条(Line)。并将关系(relation)放到线条(Line)的relations数组属性中，一条线条(Line)上可以附着多条关系(relation)。
    <br data-v-99ca356a="" />
    线条可以通过this.$refs.seeksRelationGraph.getLines()的方式获取。
  </div>
  <div data-v-99ca356a="">
    <div data-v-99ca356a="" class="c-doc-title">
      Link Arributes / 关系数据的选项:
    </div>
    <div
      data-v-99ca356a=""
      class="el-table el-table--fit el-table--enable-row-hover el-table--enable-row-transition el-table--medium"
      style="width: 100%"
    >
      <div class="el-table__body-wrapper is-scrolling-none">
        <table
          cellspacing="0"
          cellpadding="0"
          border="0"
          class="el-table__body"
          style="width: 1137px"
        >
          <thead class="has-gutter">
            <tr class="">
              <th
                colspan="1"
                rowspan="1"
                class="el-table_36_column_134 is-leaf"
              >
                <div class="cell">参数</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_36_column_135 is-leaf"
              >
                <div class="cell">说明</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_36_column_136 is-leaf"
              >
                <div class="cell">类型</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_36_column_137 is-leaf"
              >
                <div class="cell">必选/非必选</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_36_column_138 is-leaf"
              >
                <div class="cell">默认值</div>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_36_column_134">
                <div class="cell">from</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_135">
                <div class="cell">
                  <div data-v-99ca356a="">关系from节点的id</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_136">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_137">
                <div class="cell">是</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_138">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_36_column_134">
                <div class="cell">to</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_135">
                <div class="cell">
                  <div data-v-99ca356a="">关系to节点的id</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_136">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_137">
                <div class="cell">是</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_138">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_36_column_134">
                <div class="cell">text</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_135">
                <div class="cell"><div data-v-99ca356a="">关系文字</div></div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_136">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_137">
                <div class="cell">是</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_138">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_36_column_134">
                <div class="cell">styleClass</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_135">
                <div class="cell">
                  <div data-v-99ca356a="">节点样式class</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_136">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_137">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_138">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_36_column_134">
                <div class="cell">lineWidth</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_135">
                <div class="cell">
                  <div data-v-99ca356a="">线条粗细（像素）</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_136">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_137">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_138">
                <div class="cell">1</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_36_column_134">
                <div class="cell">lineColor</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_135">
                <div class="cell"><div data-v-99ca356a="">线条颜色</div></div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_136">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_137">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_138">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_36_column_134">
                <div class="cell">isHide</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_135">
                <div class="cell"><div data-v-99ca356a="">是否显示</div></div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_136">
                <div class="cell">boolean</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_137">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_138">
                <div class="cell">false</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_36_column_134">
                <div class="cell">lineShape</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_135">
                <div class="cell">
                  <div data-v-99ca356a="">
                    线条样式（1:直线/2:样式2/3:样式3/4:折线/5:样式5/6:样式6）<a
                      href="#/demo/line"
                      >使用示例</a
                    >
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_136">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_137">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_138">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_36_column_134">
                <div class="cell">fontColor</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_135">
                <div class="cell">
                  <div data-v-99ca356a="">线条文字颜色</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_136">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_137">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_138">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_36_column_134">
                <div class="cell">showLineLabel</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_135">
                <div class="cell">
                  <div data-v-99ca356a="">是否显示连线文字</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_136">
                <div class="cell">boolean</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_137">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_138">
                <div class="cell">true</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_36_column_134">
                <div class="cell">isHideArrow</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_135">
                <div class="cell">
                  <div data-v-99ca356a="">是否显示箭头</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_136">
                <div class="cell">boolean</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_137">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_138">
                <div class="cell">true</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_36_column_134">
                <div class="cell">data</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_135">
                <div class="cell">
                  <div data-v-99ca356a="">
                    <span style="color: red"
                      >自定义属性需要放在这里，才能在后续使用中从节点获取，如:{myKey1:'value1',myKey2:'value2'}</span
                    >,示例1：<a href="#/demo/adv-data-filter"
                      >使用自定义属性进行筛选</a
                    >
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_136">
                <div class="cell">Object</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_137">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_36_column_138">
                <div class="cell"></div>
              </td>
            </tr>
            <!---->
          </tbody>
        </table>
        <!----><!---->
      </div>
      <!----><!----><!----><!---->
      <div class="el-table__column-resize-proxy" style="display: none"></div>
    </div>
  </div>
  <div data-v-99ca356a="">
    <div data-v-99ca356a="" class="c-doc-title">Line / 线条对象属性:</div>
    <div
      data-v-99ca356a=""
      class="el-table el-table--fit el-table--enable-row-hover el-table--enable-row-transition el-table--medium"
      style="width: 100%"
    >
      <div class="el-table__body-wrapper is-scrolling-none">
        <table
          cellspacing="0"
          cellpadding="0"
          border="0"
          class="el-table__body"
          style="width: 1137px"
        >
          <thead class="has-gutter">
            <tr class="">
              <th
                colspan="1"
                rowspan="1"
                class="el-table_37_column_139 is-leaf"
              >
                <div class="cell">属性名</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_37_column_140 is-leaf"
              >
                <div class="cell">说明</div>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_37_column_139">
                <div class="cell"><div data-v-99ca356a="">relations</div></div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_37_column_140">
                <div class="cell">
                  <div data-v-99ca356a="">
                    【只读属性】Array&lt;Link&gt;这是一个数组，可以获取这个线条上所有的关系数据。
                  </div>
                </div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_37_column_139">
                <div class="cell"><div data-v-99ca356a="">fromNode</div></div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_37_column_140">
                <div class="cell">
                  <div data-v-99ca356a="">
                    【只读属性】获取这个线条的起始节点对象
                  </div>
                </div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_37_column_139">
                <div class="cell"><div data-v-99ca356a="">toNode</div></div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_37_column_140">
                <div class="cell">
                  <div data-v-99ca356a="">
                    【只读属性】获取这个线条的终止节点对象
                  </div>
                </div>
              </td>
            </tr>
            <!---->
          </tbody>
        </table>
        <!----><!---->
      </div>
      <!----><!----><!----><!---->
    </div>
  </div>
</div>

### 4. layout参数

<div data-v-15c3303f="" data-v-92459f82="">
  <div data-v-15c3303f="">
    <div data-v-15c3303f="" class="c-doc-title">所有布局都具有的选项:</div>
    <div
      data-v-15c3303f=""
      class="el-table el-table--fit el-table--enable-row-hover el-table--enable-row-transition el-table--medium"
      style="width: 100%"
    >
      <div class="el-table__body-wrapper is-scrolling-none">
        <table
          cellspacing="0"
          cellpadding="0"
          border="0"
          class="el-table__body"
          style="width: 1137px"
        >
          <thead class="has-gutter">
            <tr class="">
              <th
                colspan="1"
                rowspan="1"
                class="el-table_38_column_141 is-leaf"
              >
                <div class="cell">参数</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_38_column_142 is-leaf"
              >
                <div class="cell">说明</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_38_column_143 is-leaf"
              >
                <div class="cell">类型</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_38_column_144 is-leaf"
              >
                <div class="cell">必选/非必选</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_38_column_145 is-leaf"
              >
                <div class="cell">默认值</div>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_38_column_141">
                <div class="cell">
                  <div data-v-15c3303f="">layoutLabel</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_142">
                <div class="cell">
                  <div data-v-15c3303f="">
                    布局描述（如果有多个布局可以切换，此属性将作为布局名称显示）
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_143">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_144">
                <div class="cell">是</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_145">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_38_column_141">
                <div class="cell"><div data-v-15c3303f="">layoutName</div></div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_142">
                <div class="cell">
                  <div data-v-15c3303f="">
                    布局方式（tree树状布局/center中心布局/force自动布局）
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_143">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_144">
                <div class="cell">是</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_145">
                <div class="cell">center</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_38_column_141">
                <div class="cell">
                  <div data-v-15c3303f="">layoutClassName</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_142">
                <div class="cell">
                  <div data-v-15c3303f="">
                    当使用这个布局时，会将此样式添加到图谱上
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_143">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_144">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_145">
                <div class="cell">1</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_38_column_141">
                <div class="cell">
                  <div data-v-15c3303f="">useLayoutStyleOptions</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_142">
                <div class="cell">
                  <div data-v-15c3303f="">
                    是否使用该布局的样式设置覆盖全局样式设置（当有多个布局可供切换时，此功能可以实现不同布局下整体样式的切换）
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_143">
                <div class="cell">boolean</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_144">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_145">
                <div class="cell">false</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_38_column_141">
                <div class="cell">
                  <div data-v-15c3303f="">defaultNodeShape</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_142">
                <div class="cell">
                  <div data-v-15c3303f="">
                    当useLayoutStyleOptions=true时有效，默认的节点形状，0:圆形；1:矩形
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_143">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_144">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_145">
                <div class="cell">1</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_38_column_141">
                <div class="cell">
                  <div data-v-15c3303f="">defaultNodeColor</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_142">
                <div class="cell">
                  <div data-v-15c3303f="">
                    当useLayoutStyleOptions=true时有效，默认的节点背景颜色
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_143">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_144">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_145">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_38_column_141">
                <div class="cell">
                  <div data-v-15c3303f="">defaultNodeFontColor</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_142">
                <div class="cell">
                  <div data-v-15c3303f="">
                    当useLayoutStyleOptions=true时有效，默认的节点文字颜色
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_143">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_144">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_145">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_38_column_141">
                <div class="cell">
                  <div data-v-15c3303f="">defaultNodeBorderColor</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_142">
                <div class="cell">
                  <div data-v-15c3303f="">
                    当useLayoutStyleOptions=true时有效，默认的节点边框颜色
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_143">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_144">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_145">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_38_column_141">
                <div class="cell">
                  <div data-v-15c3303f="">defaultNodeBorderWidth</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_142">
                <div class="cell">
                  <div data-v-15c3303f="">
                    当useLayoutStyleOptions=true时有效，默认的节点边框粗细（像素）
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_143">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_144">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_145">
                <div class="cell">1</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_38_column_141">
                <div class="cell">
                  <div data-v-15c3303f="">defaultNodeWidth</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_142">
                <div class="cell">
                  <div data-v-15c3303f="">
                    当useLayoutStyleOptions=true时有效，默认的节点宽度
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_143">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_144">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_145">
                <div class="cell">自动</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_38_column_141">
                <div class="cell">
                  <div data-v-15c3303f="">defaultNodeHeight</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_142">
                <div class="cell">
                  <div data-v-15c3303f="">
                    当useLayoutStyleOptions=true时有效，默认的节点高度
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_143">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_144">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_145">
                <div class="cell">自动</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_38_column_141">
                <div class="cell">
                  <div data-v-15c3303f="">defaultExpandHolderPosition</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_142">
                <div class="cell">
                  <div data-v-15c3303f="">
                    当useLayoutStyleOptions=true时有效，默认的节点展开/关闭按钮位置（left/top/right/bottom）
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_143">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_144">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_145">
                <div class="cell">hide</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_38_column_141">
                <div class="cell">
                  <div data-v-15c3303f="">defaultLineColor</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_142">
                <div class="cell">
                  <div data-v-15c3303f="">
                    当useLayoutStyleOptions=true时有效，默认的线条颜色
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_143">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_144">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_145">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_38_column_141">
                <div class="cell">
                  <div data-v-15c3303f="">defaultLineWidth</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_142">
                <div class="cell">
                  <div data-v-15c3303f="">
                    当useLayoutStyleOptions=true时有效，默认的线条粗细（像素）
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_143">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_144">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_145">
                <div class="cell">1</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_38_column_141">
                <div class="cell">
                  <div data-v-15c3303f="">defaultLineShape</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_142">
                <div class="cell">
                  <div data-v-15c3303f="">
                    当useLayoutStyleOptions=true时有效，默认的线条样式（1:直线/2:样式2/3:样式3/4:折线/5:样式5/6:样式6）<a
                      href="/#/demo/line"
                      >使用示例</a
                    >
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_143">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_144">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_145">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_38_column_141">
                <div class="cell">
                  <div data-v-15c3303f="">defaultLineMarker</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_142">
                <div class="cell">
                  <div data-v-15c3303f="">
                    当useLayoutStyleOptions=true时有效，默认的线条箭头样式，示例参考：<a
                      href="#/options-tools"
                      >配置工具</a
                    >中的选项：连线箭头样式
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_143">
                <div class="cell">Object</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_144">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_145">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_38_column_141">
                <div class="cell">
                  <div data-v-15c3303f="">centerOffset_x</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_142">
                <div class="cell">
                  <div data-v-15c3303f="">根节点x坐标偏移量</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_143">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_144">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_145">
                <div class="cell">0</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_38_column_141">
                <div class="cell">
                  <div data-v-15c3303f="">centerOffset_y</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_142">
                <div class="cell">
                  <div data-v-15c3303f="">根节点y坐标偏移量</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_143">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_144">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_145">
                <div class="cell">0</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_38_column_141">
                <div class="cell">
                  <div data-v-15c3303f="">defaultShowLineLabel</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_142">
                <div class="cell">
                  <div data-v-15c3303f="">
                    当useLayoutStyleOptions=true时有效，默认是否显示连线文字
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_143">
                <div class="cell">boolean</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_144">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_38_column_145">
                <div class="cell">true</div>
              </td>
            </tr>
            <!---->
          </tbody>
        </table>
        <!----><!---->
      </div>
      <!----><!----><!----><!---->
      <div class="el-table__column-resize-proxy" style="display: none"></div>
    </div>
  </div>
  <div data-v-15c3303f="">
    <div data-v-15c3303f="" class="c-doc-title">
      tree / 树状布局 特有的选项:
    </div>
    <div
      data-v-15c3303f=""
      class="el-table el-table--fit el-table--enable-row-hover el-table--enable-row-transition el-table--medium"
      style="width: 100%"
    >
      <div class="el-table__body-wrapper is-scrolling-none">
        <table
          cellspacing="0"
          cellpadding="0"
          border="0"
          class="el-table__body"
          style="width: 1137px"
        >
          <thead class="has-gutter">
            <tr class="">
              <th
                colspan="1"
                rowspan="1"
                class="el-table_39_column_146 is-leaf"
              >
                <div class="cell">参数</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_39_column_147 is-leaf"
              >
                <div class="cell">说明</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_39_column_148 is-leaf"
              >
                <div class="cell">类型</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_39_column_149 is-leaf"
              >
                <div class="cell">必选/非必选</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_39_column_150 is-leaf"
              >
                <div class="cell">默认值</div>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_39_column_146">
                <div class="cell"><div data-v-15c3303f="">from</div></div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_39_column_147">
                <div class="cell">
                  <div data-v-15c3303f="">
                    left:从左到右/top:从上到下/right:从右到左/bottom:从下到上
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_39_column_148">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_39_column_149">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_39_column_150">
                <div class="cell">left</div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_39_column_146">
                <div class="cell">
                  <div data-v-15c3303f="">min_per_width</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_39_column_147">
                <div class="cell">
                  <div data-v-15c3303f="">
                    节点距离限制:节点之间横向距离最小值
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_39_column_148">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_39_column_149">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_39_column_150">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_39_column_146">
                <div class="cell">
                  <div data-v-15c3303f="">max_per_width</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_39_column_147">
                <div class="cell">
                  <div data-v-15c3303f="">
                    节点距离限制:节点之间横向距离最大值
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_39_column_148">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_39_column_149">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_39_column_150">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_39_column_146">
                <div class="cell">
                  <div data-v-15c3303f="">min_per_height</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_39_column_147">
                <div class="cell">
                  <div data-v-15c3303f="">
                    节点距离限制:节点之间纵向距离最小值
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_39_column_148">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_39_column_149">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_39_column_150">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_39_column_146">
                <div class="cell">
                  <div data-v-15c3303f="">max_per_height</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_39_column_147">
                <div class="cell">
                  <div data-v-15c3303f="">
                    节点距离限制:节点之间纵向距离最大值
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_39_column_148">
                <div class="cell">int</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_39_column_149">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_39_column_150">
                <div class="cell"></div>
              </td>
            </tr>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_39_column_146">
                <div class="cell">
                  <div data-v-15c3303f="">levelDistance</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_39_column_147">
                <div class="cell">
                  <div data-v-15c3303f="">
                    分别设置每一层的高度，例如：100,200,300,200，此设置优先级高于"节点距离限制"选项
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_39_column_148">
                <div class="cell">string</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_39_column_149">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_39_column_150">
                <div class="cell"></div>
              </td>
            </tr>
            <!---->
          </tbody>
        </table>
        <!----><!---->
      </div>
      <!----><!----><!----><!---->
      <div class="el-table__column-resize-proxy" style="display: none"></div>
    </div>
  </div>
  <div data-v-15c3303f="">
    <div data-v-15c3303f="" class="c-doc-title">
      center / 中心布局 特有的选项:
    </div>
    <div
      data-v-15c3303f=""
      class="el-table el-table--fit el-table--enable-row-hover el-table--enable-row-transition el-table--medium"
      style="width: 100%"
    >
      <div class="el-table__body-wrapper is-scrolling-none">
        <table
          cellspacing="0"
          cellpadding="0"
          border="0"
          class="el-table__body"
          style="width: 1137px"
        >
          <thead class="has-gutter">
            <tr class="">
              <th
                colspan="1"
                rowspan="1"
                class="el-table_40_column_151 is-leaf"
              >
                <div class="cell">参数</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_40_column_152 is-leaf"
              >
                <div class="cell">说明</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_40_column_153 is-leaf"
              >
                <div class="cell">类型</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_40_column_154 is-leaf"
              >
                <div class="cell">必选/非必选</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_40_column_155 is-leaf"
              >
                <div class="cell">默认值</div>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr class="el-table__row">
              <td rowspan="1" colspan="1" class="el-table_40_column_151">
                <div class="cell">
                  <div data-v-15c3303f="">distance_coefficient</div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_40_column_152">
                <div class="cell">
                  <div data-v-15c3303f="">
                    层级距离系数，默认为1，可以为小数，用于调节中心布局不同层级之间的距离，实现连线比较长的视觉效果<a
                      href="/#/demo/distance_coefficient"
                      >使用示例</a
                    >
                  </div>
                </div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_40_column_153">
                <div class="cell">number</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_40_column_154">
                <div class="cell">否</div>
              </td>
              <td rowspan="1" colspan="1" class="el-table_40_column_155">
                <div class="cell">left</div>
              </td>
            </tr>
            <!---->
          </tbody>
        </table>
        <!----><!---->
      </div>
      <!----><!----><!----><!---->
      <div class="el-table__column-resize-proxy" style="display: none"></div>
    </div>
  </div>
  <div data-v-15c3303f="">
    <div data-v-15c3303f="" class="c-doc-title">
      force / 力学自动布局 特有的选项:
    </div>
    <div
      data-v-15c3303f=""
      class="el-table el-table--fit el-table--enable-row-hover el-table--medium"
      style="width: 100%"
    >
      <div class="hidden-columns">
        <div data-v-15c3303f=""></div>
        <div data-v-15c3303f=""></div>
        <div data-v-15c3303f=""></div>
        <div data-v-15c3303f=""></div>
        <div data-v-15c3303f=""></div>
      </div>
      <div class="el-table__header-wrapper">
        <table
          cellspacing="0"
          cellpadding="0"
          border="0"
          class="el-table__header"
          style="width: 1137px"
        >
          <colgroup>
            <col name="el-table_41_column_156" width="250" />
            <col name="el-table_41_column_157" width="587" />
            <col name="el-table_41_column_158" width="100" />
            <col name="el-table_41_column_159" width="100" />
            <col name="el-table_41_column_160" width="100" />
            <col name="gutter" width="0" />
          </colgroup>
          <thead class="has-gutter">
            <tr class="">
              <th
                colspan="1"
                rowspan="1"
                class="el-table_41_column_156 is-leaf"
              >
                <div class="cell">参数</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_41_column_157 is-leaf"
              >
                <div class="cell">说明</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_41_column_158 is-leaf"
              >
                <div class="cell">类型</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_41_column_159 is-leaf"
              >
                <div class="cell">必选/非必选</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_41_column_160 is-leaf"
              >
                <div class="cell">默认值</div>
              </th>
              <th class="gutter" style="width: 0px; display: none"></th>
            </tr>
          </thead>
        </table>
      </div>
      <div class="el-table__body-wrapper is-scrolling-none">
        <table
          cellspacing="0"
          cellpadding="0"
          border="0"
          class="el-table__body"
          style="width: 1137px"
        >
          <colgroup>
            <col name="el-table_41_column_156" width="250" />
            <col name="el-table_41_column_157" width="587" />
            <col name="el-table_41_column_158" width="100" />
            <col name="el-table_41_column_159" width="100" />
            <col name="el-table_41_column_160" width="100" />
          </colgroup>
          <tbody>
            <!---->
          </tbody>
        </table>
        <div class="el-table__empty-block" style="height: 100%; width: 1137px">
          <span class="el-table__empty-text">暂无数据</span>
        </div>
        <!---->
      </div>
      <!----><!----><!----><!---->
      <div class="el-table__column-resize-proxy" style="display: none"></div>
    </div>
  </div>
  <div data-v-15c3303f="">
    <div data-v-15c3303f="" class="c-doc-title">fixed / 固定坐标布局:</div>
    <div data-v-15c3303f="" style="padding: 10px; color: rgb(102, 102, 102)">
      固定坐标布局，需要你为每一个节点设置x,y坐标，x,y坐标的值是以图谱左上角为0,0的坐标系，示例:<a
        data-v-15c3303f=""
        href="/#/demo/layout-diy"
        style="color: blue"
        >使用示例</a
      >
    </div>
    <div
      data-v-15c3303f=""
      class="el-table el-table--fit el-table--enable-row-hover el-table--medium"
      style="width: 100%"
    >
      <div class="hidden-columns">
        <div data-v-15c3303f=""></div>
        <div data-v-15c3303f=""></div>
        <div data-v-15c3303f=""></div>
        <div data-v-15c3303f=""></div>
        <div data-v-15c3303f=""></div>
      </div>
      <div class="el-table__header-wrapper">
        <table
          cellspacing="0"
          cellpadding="0"
          border="0"
          class="el-table__header"
          style="width: 1137px"
        >
          <colgroup>
            <col name="el-table_42_column_161" width="250" />
            <col name="el-table_42_column_162" width="587" />
            <col name="el-table_42_column_163" width="100" />
            <col name="el-table_42_column_164" width="100" />
            <col name="el-table_42_column_165" width="100" />
            <col name="gutter" width="0" />
          </colgroup>
          <thead class="has-gutter">
            <tr class="">
              <th
                colspan="1"
                rowspan="1"
                class="el-table_42_column_161 is-leaf"
              >
                <div class="cell">参数</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_42_column_162 is-leaf"
              >
                <div class="cell">说明</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_42_column_163 is-leaf"
              >
                <div class="cell">类型</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_42_column_164 is-leaf"
              >
                <div class="cell">必选/非必选</div>
              </th>
              <th
                colspan="1"
                rowspan="1"
                class="el-table_42_column_165 is-leaf"
              >
                <div class="cell">默认值</div>
              </th>
              <th class="gutter" style="width: 0px; display: none"></th>
            </tr>
          </thead>
        </table>
      </div>
      <div class="el-table__body-wrapper is-scrolling-none">
        <table
          cellspacing="0"
          cellpadding="0"
          border="0"
          class="el-table__body"
          style="width: 1137px"
        >
          <colgroup>
            <col name="el-table_42_column_161" width="250" />
            <col name="el-table_42_column_162" width="587" />
            <col name="el-table_42_column_163" width="100" />
            <col name="el-table_42_column_164" width="100" />
            <col name="el-table_42_column_165" width="100" />
          </colgroup>
          <tbody>
            <!---->
          </tbody>
        </table>
        <div class="el-table__empty-block" style="height: 100%; width: 1137px">
          <span class="el-table__empty-text">暂无数据</span>
        </div>
        <!---->
      </div>
      <!----><!----><!----><!---->
      <div class="el-table__column-resize-proxy" style="display: none"></div>
    </div>
  </div>
</div>

