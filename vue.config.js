const { defineConfig } = require('@vue/cli-service');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = defineConfig({
    transpileDependencies: true,
    configureWebpack: (config) => {
        config.plugins = config.plugins.concat([
            // 将 dll 注入到 生成的 html 模板中
            new CopyWebpackPlugin({
                patterns: [
                    {
                        from: __dirname + '/package.json',
                        to: __dirname + '/dist'
                    },
                    {
                        from: __dirname + '/README.md',
                        to: __dirname + '/dist'
                    }
                ]
            })
        ]);
    }
});
